import Vue from 'vue'
import App from './App.vue'
import router from './router'

// import Vuetify from 'vuetify'
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false
// Vue.use(Vuetify);

// require('dotenv').config()

new Vue({
  render: h => h(App),
  router,
  vuetify,
  components: { App }
}).$mount('#app')

Vue.components

// new Vue({
//   el: "#app",
//   router,
//   // store,
//   template: "<App/>",
//   components: { App }
// });