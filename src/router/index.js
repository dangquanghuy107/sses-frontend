import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import HelloWorld from '@/components/HelloWorld'
// import FileUploader from '@/components/FileUploader'
import Logout from '@/components/FileUploader'
import NotFoundComponent from '@/components/NotFoundComponent'
import Dashboard from '@/components/Dashboard'
import Mushra from '@/components/Mushra'
import Mos from '@/components/Mos'
import Int from '@/components/Int'

Vue.use(Router)

const ifAuthenticated = (to, from, next) => {
    const isAuthenticated = localStorage.getItem('token')
    if (isAuthenticated)
        next()
    else 
        next('/login')
}

const ifNotAuthenticated = (to, from, next) => {
    const isAuthenticated = localStorage.getItem('token')
    if (isAuthenticated)
        next('/dashboard')
    else 
        next()
}

var router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/login',
            name: 'Login', 
            component: Login,
            beforeEnter: ifNotAuthenticated
        },
        {
            path: '/',
            name: 'HelloWorld',
            component: HelloWorld
        },
        {
            path: '/logout',
            name: 'Logout',
            component: Logout
        },
        {
            path: '/dashboard',
            name: 'Dashboard', 
            component: Dashboard,
            beforeEnter: ifAuthenticated
        },
        {
            path: '/mushra',
            name: 'MUSHRA',
            component: Mushra
        },
        {
            path: '/mos',
            name: 'Mos',
            component: Mos
        },
        {
            path: '/int',
            name: 'Int', 
            component: Int
        },
        {
            path: '*',
            name: 'NotFoundComponent',
            component: NotFoundComponent
        }
    ]
})

export default router